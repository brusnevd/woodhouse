function qBlock() {
    let modal = document.querySelector(".questionModal");
    let wrappermodal = document.querySelector(".questionModal-wrapper");
    let bgModal = document.querySelector(".questionModalBg");
    let btnSubmit = document.getElementById("q-btn-submit")
    let realBtnSubmit = document.getElementById("question_submit");
    let continueBtn = document.getElementById("q-continue-btn");
    let form = document.querySelector(".question-form-afrom");
    let questionsInputs = document.getElementsByClassName("questions-form__input");
    let questionsArea = document.querySelector(".questions-form__area");

    function showDarkBg() {
        bgModal.classList.remove("modal-bg-animation-out");
        bgModal.classList.add("modal-bg-animation-in");
        document.body.style.overflow = "hidden";
    }

    function hideDarkBg() {
        bgModal.classList.add("modal-bg-animation-out");
        bgModal.classList.remove("modal-bg-animation-in");
    }

    function showModal() {
        modal.classList.remove("question-block-animation-out");
        modal.classList.add("question-block-animation-in");
        wrappermodal.classList.remove("display-none");
        wrappermodal.classList.add("display-block");
        showDarkBg();
    }

    function hideModal() {
        modal.classList.remove("question-block-animation-in");
        modal.classList.add("question-block-animation-out");
        modal.addEventListener("animationend", removeModalAnimationOut);
        
        document.body.style.overflow = "auto";
        clearInputs();

        hideDarkBg();
    }

    function removeModalAnimationOut() {
        wrappermodal.classList.remove("display-block");
        wrappermodal.classList.add("display-none");
        modal.removeEventListener("animationend", removeModalAnimationOut);
    }

    bgModal.addEventListener('click', (e) => {
        hideModal();
    });

    function clearInputs() {
        for (let item of questionsInputs) {
            item.value = "";
        }

        questionsArea.value = "";
        
    }

    continueBtn.addEventListener('click', (e) => {
        hideModal();
    })

    form.addEventListener('submit', (e) => {
        showModal();
    });
}

qBlock();