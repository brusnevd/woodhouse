function burgerMenu(selector) {
    let menu = document.querySelector(selector);
    let button = document.querySelector('.burger-menu__button');
    let links = document.querySelector('.burger-menu__nav');
    let overlay = document.querySelector('.burger-menu__overlay');

    button.addEventListener('click', (e) => {
        e.preventDefault();
        toggleMenu();
    })
    
    links.addEventListener('click', () => toggleMenu());
    overlay.addEventListener('click', () => toggleMenu());

    function toggleMenu() {
        menu.classList.toggle("burger-menu_active");

        // if (menu.classList.includes("burger-menu_active")) {
        //     document.getElementsByTagName("body").style.overflow = "hidden";
        // } else {
            
        //     document.getElementsByTagName("body").style.overflow = "visible";
        // }
    }


}

burgerMenu('.burger-menu');