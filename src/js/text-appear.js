let letters = ['', '', '', '', '%', '%%', '%%%', '%%%', '%%%', '%%%', '%%%', '%%%', '%%%', '%%%', '%%%', '%%%', '%%%', '%%%', '%%', '%', ''],
    i = 0,
    elem = document.getElementById("dynamic-text").querySelector(".typed-text"),
    cursor = document.getElementById("dynamic-text").querySelector(".typed-cursor");

setInterval(() => {
    elem.innerText = letters[i++];
    if (i == 21) {
        i = 0;
    }
}, 100);
