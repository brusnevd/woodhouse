var object_kashpo = [
    {
        name: "Бело-зелёное",
        title: "Белое кашпо с зелёным мхом",
        form: "Форма - гексагон",
        description: "Каждое кашпо - ручной работы. В качестве основного сырья для кашпо используется гипс и скандинавский мох, все материалы полностью гипоаллергенны",
        additional: "размеры: высота ~ 12 см, ширина - 12",
        prise: 300,
        src: "build/img/catalog-image-1.jpg",
    },
    {
        name: "Черно белое",
        title: "Белое кашпо с жёлтым мхом",
        form: "Форма - гексагон",
        description: "Каждое кашпо - ручной работы. В качестве основного сырья для кашпо используется гипс и скандинавский мох, все материалы полностью гипоаллергенны",
        additional: "размеры: высота ~ 12 см, ширина - 12",
        prise: 300,
        src: "build/img/catalog-image-2.jpg",
    },
    {
        name: "Кросно белое",
        title: "Белое кашпо с красным мхом",
        form: "Форма - гексагон",
        description: "Каждое кашпо - ручной работы. В качестве основного сырья для кашпо используется гипс и скандинавский мох, все материалы полностью гипоаллергенны",
        additional: "размеры: высота ~ 12 см, ширина - 12",
        prise: 300,
        src: "build/img/catalog-image-3.jpg",
    },
    {
        name: "Белое",
        title: "Бело-красное кашпо с жёлтым мхом",
        form: "Форма - гексагон",
        description: "Каждое кашпо - ручной работы. В качестве основного сырья для кашпо используется гипс и скандинавский мох, все материалы полностью гипоаллергенны",
        additional: "размеры: высота ~ 12 см, ширина - 12",
        prise: 300,
        src: "build/img/catalog-image-1.jpg",
    },
    {
        name: "Черно белое",
        title: "Бело-красное кашпо с зелёным мхом",
        form: "Форма - гексагон",
        description: "Каждое кашпо - ручной работы. В качестве основного сырья для кашпо используется гипс и скандинавский мох, все материалы полностью гипоаллергенны",
        additional: "размеры: высота ~ 12 см, ширина - 12",
        prise: 300,
        src: "build/img/catalog-image-2.jpg",
    },
    {
        name: "Кросно белое",
        title: "Бело-красное кашпо с красным мхом",
        form: "Форма - гексагон",
        description: "Каждое кашпо - ручной работы. В качестве основного сырья для кашпо используется гипс и скандинавский мох, все материалы полностью гипоаллергенны",
        additional: "размеры: высота ~ 12 см, ширина - 12",
        prise: 300,
        src: "build/img/catalog-image-3.jpg",
    },
    {
        name: "Белое",
        title: "Чёрно-белое кашпо с зелёным мхом",
        form: "Форма - гексагон",
        description: "Каждое кашпо - ручной работы. В качестве основного сырья для кашпо используется гипс и скандинавский мох, все материалы полностью гипоаллергенны",
        additional: "размеры: высота ~ 12 см, ширина - 12",
        prise: 300,
        src: "build/img/catalog-image-1.jpg",
    },
    {
        name: "Черно белое",
        title: "Чёрно-белое кашпо с красным мхом",
        form: "Форма - гексагон",
        description: "Каждое кашпо - ручной работы. В качестве основного сырья для кашпо используется гипс и скандинавский мох, все материалы полностью гипоаллергенны",
        additional: "размеры: высота ~ 12 см, ширина - 12",
        prise: 300,
        src: "build/img/catalog-image-2.jpg",
    },
    {
        name: "Кросно белое",
        title: "Чёрно-белое кашпо с жёлтым мхом",
        form: "Форма - гексагон",
        description: "Каждое кашпо - ручной работы. В качестве основного сырья для кашпо используется гипс и скандинавский мох, все материалы полностью гипоаллергенны",
        additional: "размеры: высота ~ 12 см, ширина - 12",
        prise: 300,
        src: "build/img/catalog-image-3.jpg",
    },
    {
        name: "Набор из 3 кашпо",
        title: "3 кашпо",
        form: "Форма - гексагон",
        description: "Каждое кашпо - ручной работы. В качестве основного сырья для кашпо используется гипс и скандинавский мох, все материалы полностью гипоаллергенны",
        additional: "размеры: высота ~ 12 см, ширина - 12",
        prise: 300,
        src: "build/img/catalog-image-4.jpg",
    }
];

//Обработка события нажатия на кнопку сделать заказ

let makeOrder = document.querySelector(".modal-basket_btn");

makeOrder.addEventListener('click', (e) => {
    e.preventDefault();
    localStorage.removeItem("orders");
    document.body.style.position = 'relative';
    let loading = document.createElement('div');
    loading.style.width = '100%';
    loading.style.height = '100vh';
    loading.style.zIndex = '999999';
    loading.style.fontSize = '50px';
    loading.style.background = 'rgba(216, 216, 216, .5)';
    loading.style.position = 'fixed';
    loading.style.display = 'flex';
    loading.style.justifyContent = 'center';
    loading.style.alignItems = 'center';
    loading.innerText = "Loading...";
    document.body.prepend(loading);
    document.querySelector(".modal-basket_btn-submit").click();
});

function payModal() {

    //code for add item

    let modal = document.getElementById("modal-zakaz");
    let modal_content = document.getElementById("mzakaz__content");
    let butns_block = document.getElementById("mzakaz__buttons-block");
    let some_block = document.getElementById("mzakaz__product-descript");
    let btn1 = document.getElementById("mzakaz__close");
    let btn2 = document.getElementById("mzakaz__more-producs");
    let add_btn = document.querySelector(".z-product__add");

    let add_buttons = document.querySelectorAll(".catalog__item-btn .button");
    let lenth_add_buts = add_buttons.length;

    //добавление заказа

    add_btn.addEventListener("click", function() {
        let position_name = this.parentNode.children[0].innerText;
        let price = parseInt(this.parentNode.children[1].innerText, 10);
        let url_img = this.parentNode.parentNode.children[0].children[0].src;

        let obj = {
            name: position_name,
            prise: price,
            count: 1,
            url_img: url_img
        };

        let orders_mass = JSON.parse(localStorage.getItem("orders")) || [];
        let orders_mass_len = orders_mass.length;
        let flag = true;

        for (let i = 0; i < orders_mass_len; i++) {
            if (orders_mass[i].name.indexOf(position_name) != -1) {
                orders_mass[i].count = orders_mass[i].count + 1;
                flag = false;
            }
        }

        if (flag) {
            orders_mass.push(obj);
        }

        localStorage.setItem("orders", JSON.stringify(orders_mass));

        openBasketModal();
        closeModalAndrew();
        getFromLocalStorage();
        writeInfoInIconBasket();
        writeOrderInfoInBasket();

    });


    for (let i = 0; i < lenth_add_buts; i++) {
        add_buttons[i].addEventListener("click", function() {
            modal.style.display = "block";
            modal.removeEventListener("animationend", changeDisplay);
            modal.classList.add("modal-animation-in");
            modal.classList.remove("modal-animation-out");

            let name_kashpo = this.parentNode.parentNode.children[1].innerText;
            let real_price = this.parentNode.parentNode.lastChild.previousSibling.previousSibling.previousSibling.children[0].innerText;
            let img_src = this.parentNode.parentNode.children[0].children[0].src;

            let title = modal.querySelector(".z-product__title")
            let title2 = modal.querySelector(".z-product-desc__title");
            let price = modal.querySelector(".z-product__price");
            let form = modal.querySelector(".z-product-desc__form");
            let description = modal.querySelector(".z-product-desc__description");
            let additional = modal.querySelector(".z-product-desc__additional");
            let img = modal.querySelector(".mzakaz__product-img img");

            title.innerText = name_kashpo;
            title2.innerText = object_kashpo[i].title;
            price.innerText = real_price;
            form.innerText = object_kashpo[i].form;
            description.innerText = object_kashpo[i].description;
            additional.innerText = object_kashpo[i].additional;
            img.src = img_src;

            document.body.style.overflow = "hidden";
        });
    }

    function changeDisplay() {
        modal.style.display = "none";
    }

    function btn_click() {
        modal.classList.remove("modal-animation-in");
        modal.classList.add("modal-animation-out");
        modal.addEventListener("animationend", changeDisplay);
        document.body.style.overflow = "visible";
    }

    btn1.addEventListener("click", () => {
        btn_click();
    });

    btn2.addEventListener("click", () => {
        btn_click();
    });

    modal.addEventListener("click", (event) => {
        if (event.target == modal || event.target == modal_content || event.target == butns_block || event.target == some_block) {
            btn_click();
        }
    });

    //

    //code for modalbasket

    const btnShowModal = document.querySelector(".z-product__add"),
        modalBasket = document.querySelector(".modal-basket"),
        modalBasketWrapper = document.querySelector(".modal-basket-wrapper");
    modalBasketContainer = document.querySelector(".modal-basket__container"),
        btnCloseBasket = document.querySelector(".close"),
        modalAndrew = document.getElementById("modal-zakaz");
    basket = document.querySelector(".basket");
    basketTotal = document.querySelector(".basket__total");
    basketCountBlock = document.querySelector(".basket__count-block");
    productsContainer = document.querySelector(".products-container");
    totalAmount = document.querySelector(".modal-basket__price span");

    let orderMass = [];
    let fullprice = 0;
    let count_order_items = 0;

    function changeDisplay() {
        modalAndrew.style.display = "none";
        modalAndrew.removeEventListener("animationend", changeDisplay);
    }

    function changeDisplayDimasModal() {
        modalBasket.style.display = "none";
        modalBasketWrapper.style.display = "none";
        modalBasket.removeEventListener("animationend", changeDisplayDimasModal);
        document.body.style.overflow = "visible";
    }

    function openBasketModal() {
        modalBasket.style.display = "flex";
        modalBasketWrapper.style.display = "block";
        modalBasket.classList.add("modal-dimas-animation-in");
        modalBasket.classList.remove("modal-dimas-animation-out");
        document.body.style.overflow = "hidden"
    }

    function closeBasketModal() {
        modalBasket.classList.remove("modal-dimas-animation-in");
        modalBasket.classList.add("modal-dimas-animation-out");
        modalBasket.addEventListener("animationend", changeDisplayDimasModal)
    }

    function closeModalAndrew() {
        modalAndrew.classList.remove("modal-animation-in");
        modalAndrew.classList.add("modal-animation-out");
        modalAndrew.addEventListener("animationend", changeDisplay);
    }

    function getFromLocalStorage() {
        orderMass = JSON.parse(localStorage.getItem('orders')) || [];
    }

    function saveOrderToLocalStorage() {
        localStorage.setItem("orders", JSON.stringify(orderMass));
    }

    function determineTotalAmountAndCount() {
        fullprice = count_order_items = 0;
        if (orderMass.length) {
            orderMass.forEach(elem => {
                fullprice += elem.prise * elem.count;
                count_order_items += elem.count;
            });
        }
    }

    function writeInfoInIconBasket() {
        determineTotalAmountAndCount();
        if (fullprice == 0) {
            basket.style.display = "none";
        } else {
            basket.style.display = "block";
        }
        basketTotal.innerText = fullprice + " руб.";
        basketCountBlock.innerText = count_order_items;
    }

    function deleteItemFromBasket(index) {
        orderMass.splice(index, 1);
        if (orderMass.length == 0) {
            setTimeout(() => {
                closeBasketModal();
            }, 100)
        }
        saveOrderToLocalStorage();
        writeInfoInIconBasket();
        writeOrderInfoInBasket();
    }

    function increaseCountItem(index) {
        orderMass[index].count = orderMass[index].count + 1;
        saveOrderToLocalStorage();
        getFromLocalStorage();
        writeInfoInIconBasket();
        writeOrderInfoInBasket();
    }

    function decreaseCountItem(index) {
        orderMass[index].count = orderMass[index].count - 1;
        saveOrderToLocalStorage();
        getFromLocalStorage();
        writeInfoInIconBasket();
        writeOrderInfoInBasket();
    }

    function writeOrderInfoInBasket() {
        productsContainer.innerHTML = "";
        let total_amount = 0;
        orderMass.forEach((elem, index) => {
            let delete_btn = document.createElement("div");
            delete_btn.classList.add("btn-close");
            delete_btn.innerHTML = `<img src="https://static.tildacdn.com/lib/linea/1bec3cd7-e9d1-2879-5880-19b597ef9f1a/arrows_circle_remove.svg" style="width:20px;height:20px;border:0;">`
            delete_btn.addEventListener("click", () => {
                deleteItemFromBasket(index);
            });

            //structure

            let for_media = document.createElement('div');
            for_media.classList.add("for-media");

            // let product_title = document.createElement("input");
            // product_title.classList.add("product-title");
            // product_title.setAttribute('readonly', 'true');
            // product_title.name = "titlearray[]";
            // product_title.value = elem.name;

            let product_title = document.createElement("textarea");
            product_title.classList.add("product-title");
            product_title.setAttribute('readonly', 'true');
            product_title.name = "titlearray[]";
            product_title.value = elem.name;
            product_title.style.resize = 'none';
            product_title.style.overflow = 'hidden';
            product_title.style.width = '80%';
            product_title.style.textAlign = 'center';

            let for_media_container = document.createElement("div");
            for_media_container.classList.add("for-media-container");

            let product_count = document.createElement("div");
            product_count.classList.add("product-count");

            let minus_btn = document.createElement("div");
            minus_btn.classList.add("less");
            minus_btn.innerText = "-";
            minus_btn.addEventListener("click", () => {
                if (elem.count == 1) {
                    deleteItemFromBasket(index);
                } else {
                    decreaseCountItem(index);
                }
            });

            let count = document.createElement("input");
            count.classList.add("count");
            count.name = "countarray[]";
            console.log(elem.count);
            count.value = elem.count;

            let plus_btn = document.createElement("div");
            plus_btn.classList.add("more");
            plus_btn.innerText = "+";

            plus_btn.addEventListener("click", () => {
                increaseCountItem(index);
            });

            let products__item = document.createElement("div");
            products__item.classList.add("products__item");

            product_count.append(minus_btn);
            product_count.append(count);
            product_count.append(plus_btn);

            countitems = elem.prise * elem.count + ' руб.';

            for_media_container.innerHTML = '<input class="product-price" name="pricesarray[]" value = "' + countitems + '" readonly>';
            for_media_container.insertAdjacentElement("afterBegin", product_count);

            for_media.insertAdjacentElement("beforeEnd", product_title);
            for_media.insertAdjacentElement("beforeEnd", for_media_container);

            products__item.innerHTML = `<div class="product-image">
                                            <img src="${elem.url_img}" alt="clocks">
                                        </div>`

            products__item.insertAdjacentElement("beforeEnd", for_media);

            products__item.insertAdjacentElement('beforeEnd', delete_btn);
            productsContainer.insertAdjacentElement('beforeEnd', products__item);

            total_amount += elem.count * elem.prise
        });

        totalAmount.innerText = total_amount + " руб.";


        // $(".less").on("click", (event) => {

        //     let text = Number($(event.target).siblings(".count").text());

        //     if (text !== 0) {
        //         $(event.target).siblings(".count").text(text - 1);
        //     }

        // })  

        // $(".more").on("click", () => {
        //     let text = Number($(event.target).siblings(".count").text());
        //     $(event.target).siblings(".count").text(text + 1);
        // })
    }

    getFromLocalStorage();
    writeInfoInIconBasket();
    writeOrderInfoInBasket();

    basket.addEventListener("click", () => {
        openBasketModal();
    });

    modalBasket.addEventListener("click", () => {
        if ((event.target != modalBasketContainer && event.target == modalBasket) || event.target == btnCloseBasket) {
            closeBasketModal();
        }
    });
}

payModal();